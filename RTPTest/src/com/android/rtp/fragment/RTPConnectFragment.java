package com.android.rtp.fragment;

import java.net.DatagramSocket;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.common.utils.DialogUtils;
import com.android.common.utils.LogUtil;
import com.example.rtptest.R;
import com.java.jlibrtp.DataFrame;
import com.java.jlibrtp.Participant;
import com.java.jlibrtp.RTPAppIntf;
import com.java.jlibrtp.RTPSession;

public class RTPConnectFragment extends Fragment implements RTPAppIntf {
	private static final String TAG = "RTPCONNECT　TEST　";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_rtpconnec_test,
				container, false);
		initView(rootView);
		return rootView;
	}

	private EditText mHostEt, mPortEt;
	private Context mContext;

	private void initView(View v) {
		mHostEt = (EditText) v.findViewById(R.id.et_host);
		mPortEt = (EditText) v.findViewById(R.id.et_port);
		v.findViewById(R.id.bt_sure).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String host = mHostEt.getText().toString();
				if (TextUtils.isEmpty(host)) {
					DialogUtils.showShortToast(mContext, "请先输入主机名");
					return;
				}
				String s_port = mPortEt.getText().toString();
				if (TextUtils.isEmpty(s_port)) {
					DialogUtils.showShortToast(mContext, "请先输入端口");
					return;
				}

				int i_port = 0;
				try {
					i_port = Integer.valueOf(s_port);
				} catch (NumberFormatException e) {
					DialogUtils.showShortToast(mContext, "请先输数字端口");
					return;
				}

				connect(host, i_port);

			}
		});
	}

	private void connect(String host, final int port) {
	
	LogUtil.e(TAG, " connect ");
		
		new Thread(){
			public void run(){
				
				DatagramSocket rtpSocket = null;
				DatagramSocket rtcpSocket = null;

				try {
					rtpSocket = new DatagramSocket(port);
					rtcpSocket = new DatagramSocket(port+1);
				} catch (Exception e) {
					
					LogUtil.e(TAG, "创建socket失败 "+e.toString());
//					DialogUtils.showShortToast(mContext, "创建socket失败");
					return;
				}

				RTPSession rtpSession = new RTPSession(rtpSocket, rtcpSocket);
				rtpSession.naivePktReception(true);
				rtpSession.RTPSessionRegister(RTPConnectFragment.this, null, null);
				
			}
		}.start();

	

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
	}

	@Override
	public void receiveData(DataFrame frame, Participant participant) {
		LogUtil.e(TAG, "receiveData :  frame is "+frame);

	}

	@Override
	public void userEvent(int type, Participant[] participant) {
		LogUtil.e(TAG, "userEvent :  type is "+type);

	}

	@Override
	public int frameSize(int payloadType) {
		LogUtil.e(TAG, "frameSize :  payloadType is "+payloadType);
		return 0;
	}

}
